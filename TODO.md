# GSoC 2020 TODO list

This is a list of features, bugs, and known issues for my GSoC project. Strikethrough means implemented.
However, this might be only on my local machine, not even on the [Gitlab branch](https://gitlab.com/vanntile/inkscape/-/commits/docking-system-copy).

## Potential Features
1. Toolbars resizable and dockable (without notebooks)
2. Close floating dialogs when hitting escape (controversial, maybe on the same shortcut - but this is hard to implement)
3. Reopening closed dialog with Ctrl+Shift+T (like in a browser)
4. DialogNotebook should scroll content only (sticky header)
5. Adding dialogs below the canvas
6. Dragging notebook to reposition
7. Dockable canvas.
8. DialogMultipaned locked sizes

## Potential microprojects
1. Migrating from verbs to actions in the DialogContainer
2. Removing the empty_widget from DialogMultipaned if proven just a code creep with no future use (now that there is no moment when an empty column is visible). Here we could also destroy an empty DialogMultipaned that is the child of another (moved from DialogContainer::column_empty())
3. Adding dialog state profiles and loading them on the fly. (UI & functionality)
4. Saving dialog positions and sizes along with configurations.

## Known issues
1. The shortcuts to open a new dialog don't work when a floating window is focused
2. UX problem : when you have too many tabs (+5) in one notebook and they get hidden there is no way how to even see there is more tabs hidden. this is likely to happen novice users if they open to many dialogs. [report link with details](https://gitlab.com/inkscape/ux/-/issues/38#note_395581246)
3. Rulers and scrollbars show when you add a docked dialog from a clean state
4. Docked dialogs are not sized properly initially
5. Drag & dropping dialog tabs to undefined areas doesn't work consistently (`Windows` & `Wayland`)
6. DialogNotebook misses borders on light themes
7. When switching tabs, the notbook jumps down

## Bugs
1. When draging a handle fast (until the screen edge) in order to resize to 0, the child snaps to a large size
2. `Windows` opening dialog from shortcut (eg Layers) doesn't render it, but if you minimize & maximize, it does. (Trace: `(org.inkscape.Inkscape:21332): Gtk-WARNING **: 19:00:57.556: Drawing a gadget with negative dimensions. Did you forget to allocate a size? (node tab owner gtkmm__GtkNotebook)`)
3.  XML Editor Dialog has an empty input string that highlights when tab dragging
4.  Document Properties sticks to being docked after being opened once
5.  Dialog rearranging leads to crashes. (needs reproducing)
