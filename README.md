# Inkscape GSoC 2020 application draft

This is my 2020 Inkscape GSoC application for **GDL refactoring**.

## About myself.

My name is Valentin Ioniță (aka `vanntile`) and I am a junior student in
Computer Science and Engineering at University Politehnica of Bucharest, Romania,
south-eastern Europe. I have been studying my third year as an Erasmus exchange
student in Åbo Akademi University, Turku, Finland and I am there as I type this.

My main focus as a developer is in web development and graphic design, being
self-taught in both domains, while being educated as a generalist computer
engineer in university. I have successfully participated to [last year's GSoC][1],
having contributed to Inkscape by increasing browser compatibility with
Inkscape files by writing JavaScript Polyfills and creating a new piece of
dialog. In the last year, I have been broadening my skills toolkit with two
web-frameworks (React and Django) and a deep understanding of Data Science and
Machine Learning, with practical skills of data-analysis in Python.

Five years ago I started contributing on StackOverflow (now having over 2k
points) and stumbled upon the open-source community. A few of years ago,
along with starting college, I finally grasped the value of open-source and
its importance to the technology world. Now I am fully aware of the value of
the open-source movement in both development and design which spawns a great
number of projects upon which the tech industry stands upon. I am interested
in making a difference myself, bit by bit, in different parts of the stack.

I am consistently building up my skills and believe that in order to contribute
to projects and communities that I find exciting, there is more that can be
done together rather than independently. Inkscape is one of those projects, as
I have repeatedly used the application to practice design, create assets for
student events and help people that needed graphics.


### Summer plans

For this summer I am actively pursuing a few goals:

- reading one hour a day, to complete my fourth [Goodreads reading challange][2]
- learning CI/CD, GitHub Actions and setting up a private GitLab environment
- creating a conversational bot running in Google assistant
- researching potential bachelor thesis topics

### Contact info

You can check out my [resume][3] or my tech/social [platforms][4]. You can find
me on most social platforms and Inkscape's Rochet Chat with the handle
`@vanntile`.

- [LinkedIn][5]
- [Github][6] where you can check, for example, my collaboration on a full-stack
[web app][7] destined for Oura ring users (Flask backend and Angular frontend)
or my progress into a [privacy in machine learning course][8] from Andrew Trask.
- [StackOverflow][9]


### Tool Stack

I have or I am currently using

- Programming Languages: C/C++, JavaScript (ES2019), TypeScript, Python,
Haskell, Matlab, SQL
- Editors: Visual Studio Code (current), the JetBrains stack (IntelliJ, PyCharm,
CLion), gdb, IDA Dissasembler, Vim
- Grapics: Inkscape, Figma, GIMP
- Versioning: Git; platforms: Github, Gitlab, Bitbucket
- I am an Ubuntu 18.04 user, and my browser of choice is Firefox

### Other GSoC

Other than Inkscape, where I'm driven by passion and a sense of community, I
will apply to Moira, where there is a specific frontend project that fits my
skillset.


## How I use Inkscape

You can see most of my graphics work on [Instagram][10] and [Dribbble][11] and
should check out how I turned a team into a family using Inkscape (the story and
design process can be found [here][12] or how I simulated [grain textures][13]
in Inkscape.

You can find some of my source files (or the `.png` exports) in the [graphics][14]
folder of the repo. Here is a small preview.

![cover][15]

I was a graphic design volunteer for the student association in my college and
I have worked on various projects in the last couple of years, getting to be a
design proect manager for a student hackathon. You can view my work in this
repo’s [LSAC][16] folder. Working using Inkscape has raised some compatibility
issues with closed software such as Adobe Illustrator, but it all worked out
and, at the beginning of last's school year, I have co-held a design workshop
and plan doing another one on typography soon. I had the opportunity of teaching
and leading students to design and it’s all thanks to hard work and Inkscape.

## Why me?

I know that this year, Inkscape has a large number of participants who are
eager to bring the best out of their work and contribute to the betterment of
Inkscape. While that is to admire and I know Inkscape can request only a
limited number of projects, I know that the project I'm proposing helps
reducing bugs and modernising code which highly helps both the application
quality and the post 1.0 development process. I already have a good
relationship with Tav, my last GSoC's mentor, so the collaboration would work
in an efficient manner.

## Project proposal

I want to propose **Gnome Docking Library (GDL) refactoring**, based on a demo
from Tav found [here][17]. This would mean creating a multipane widget that
docks different dialogs into "notebooks", each notebook being created on the fly,
having at least one dialog and being in sync with the document it belongs to.

The goals of this proposal would be the following:

- removing GDL as a dependency
- creating a **Multipane** widget that manages all types of dialogs
- saving dialog state
- update dialog state though a new system pushing forward Gio::Actions instead
of Gtk::Actions, supporting the idea of a future headless Inkscape.

The value of this proposal stands in three points:

- modernisation (removing old dependencies)
- standardisation (making all dialogs have a template to follow)
- synchronisation (exposing the decoupling of the document operations from the GUI)

Proposed timeline:

- [ ] Week 0: (25th-31st May) Working on the demo version, adding state saving
- [ ] Week 1-3: Documenting signaling in Inkscape and differences in dialog
implementations
- [ ] Week 4-6: Implementing the Multipane widget, creating notebooks logic and
migrating 2 dialogs
- [ ] Week 7-8: Migrating rest of the dialogs
- [ ] Week 9: Adding the state-saving features
- [ ] Week 10: Checking build pipeline, removing unused dependecies (related to
GDL)
- [ ] Week 11: Creating tests for multiple documents and signals/actions
- [ ] Week 12: (17th-23rd August) Extra bugfix week



---


[1]: https://gitlab.com/vanntile/inkscape-gsoc-application
[2]: https://www.goodreads.com/user_challenges/19341974
[3]: ./Resume_Valentin_Ionita.pdf
[4]: http:vanntile.tk
[5]: https://www.linkedin.com/in/valentin-ionita/
[6]: https://github.com/vanntile
[7]: https://github.com/Poppy22/oura-challenge
[8]: https://github.com/vanntile/secure-and-private-ai-challenge
[9]: https://stackoverflow.com/users/4679160/vanntile-ianito
[10]: https://www.instagram.com/vanntile/
[11]: https://dribbble.com/vanntile
[12]: https://www.linkedin.com/pulse/what-ifttus-valentin-ionita/
[13]: https://medium.com/vanntile-factory/design-challenge-grain-texture-in-inkscape-f6e185c4c067
[14]: ./graphics
[15]: ./graphics/cover.png
[16]: ./graphics/LSAC
[17]: https://gitlab.com/Tavmjong/gtk_sandbox/-/tree/master/gtkmm_paned

